import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_ptgits/main.dart';

class Soalnomerdua extends StatefulWidget {
  const Soalnomerdua({Key? key}) : super(key: key);

  @override
  _SoalnomerduaState createState() => _SoalnomerduaState();
}

class _SoalnomerduaState extends State<Soalnomerdua> {
  TextEditingController currentPasswordController = new TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SafeArea(
          child: Scaffold(
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 30.0),
          child: Form(
            key: _formKey,
              child: Column(
            children: [
              Divider(color: Colors.black, thickness: 2.0),
              TextFormField(
                  autovalidateMode: AutovalidateMode.always,
                  textAlign: TextAlign.center,
                  controller: currentPasswordController,
                  keyboardType: TextInputType.emailAddress,
                  // inputFormatters: <TextInputFormatter>[
                  //   FilteringTextInputFormatter.digitsOnly
                  // ],
                  validator: (value) {
                    RegExp regex = new RegExp(
                        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');

                    //  print(setelah.toString());
                    if (value!.length >= 5) {
                      String getcoid = value.substring(value.length - 5);
                      String getid = value.substring(value.length - 3);
                      if (value.isEmpty) {
                        return 'kosong';
                      } else if (!(regex.hasMatch(value))) {
                        return "invalid email";
                      }
                      else if (!getcoid.contains('.co.id') &&
                          !getid.contains('.id')) {
                        return "invalid email";
                      }
                    }else{
                      return "invalid email";
                    }
                  },
                  decoration: InputDecoration.collapsed(
                      hintText: "inputkan email, Example : .co.id or .id")),
              Divider(color: Colors.black, thickness: 2.0),
              Container(
                width: MediaQuery.of(context).size.width,
                child: TextButton(
                  style: ButtonStyleCustom.outlinedbutton,
                  onPressed: () {
                    final formState = _formKey.currentState;
                    if (formState!.validate()) {
                      // dynamic hitung1 =
                      //     int.parse(currentPasswordController.text) % 3;
                      // dynamic hitung2 =
                      //     int.parse(currentPasswordController.text) % 5;
                      // print(hitung1.toString());
                      // print(hitung2.toString());
                      // if (hitung2 == 0 && hitung1 == 0) {
                      //   print('satu');
                      // } else if (hitung1 == 0 && hitung2 != 0) {
                      //   print('dua');
                      // } else if (hitung2 == 0 && hitung1 != 0) {
                      //   print('tiga');
                      // } else {
                      //   print('tidak ada return');
                      // }
                      print('success');
                    } else {
                      return null;
                    }
                  },
                  child: Text('Submit',
                      style: TextStyle(
                          // fontFamily: 'PlusJakartaSans',
                          fontSize: 18,
                          fontWeight: FontWeight.w500)),
                ),
              ),
            ],
          )),
        ),
      )),
    );
  }
}
