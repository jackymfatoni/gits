import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_ptgits/main.dart';
import 'package:intl/intl.dart';

class Soalnomertiga extends StatefulWidget {
  const Soalnomertiga({Key? key}) : super(key: key);

  @override
  _SoalnomertigaState createState() => _SoalnomertigaState();
}

class _SoalnomertigaState extends State<Soalnomertiga> {
  TextEditingController currentPasswordController = new TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String timeStamp24HR = "2020-07-20T18:15:12";
  String? _selectedTime;
  Future<void> _show() async {
    final TimeOfDay? result = await showTimePicker(
        context: context,
        initialTime: TimeOfDay.now(),
        builder: (context, child) {
          return MediaQuery(
              data: MediaQuery.of(context).copyWith(
                  // Using 12-Hour format
                  alwaysUse24HourFormat: false),
              // If you want 24-Hour format, just change alwaysUse24HourFormat to true
              child: child!);
        });
    if (result != null) {
      setState(() {
        _selectedTime = result.format(context);

        print(
            DateFormat.jm().format(DateTime.parse(_selectedTime!)).toString());
      });
    }
  }

  _check() {
    DateFormat.jm().format(DateTime.parse(timeStamp24HR));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: SafeArea(
            child: Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 25.0),
        child: Column(
          children: [
            Form(
              key: _formKey,
              child: Column(
                children: [
                  Center(
                    child: Text(
                      _selectedTime != null
                          ? _selectedTime!
                          : 'GAGAL',
                      style: TextStyle(fontSize: 30),
                    ),
                  ),
                  ElevatedButton(
                      onPressed: _show, child: Text('Show Time Picker')),
                  Text(''),
                  Divider(color: Colors.black, thickness: 2.0),
                  TextFormField(
                      autovalidateMode: AutovalidateMode.always,
                      textAlign: TextAlign.center,
                      controller: currentPasswordController,
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.digitsOnly
                      ],
                      validator: (value) {
                        if (value!.isEmpty) {
                          //  return reverseStringUsingCodeUnits('coflutter');
                        }
                      },
                      decoration: InputDecoration.collapsed(
                          hintText: "inputkan bilangan, Example : 3")),
                  Divider(color: Colors.black, thickness: 2.0),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: TextButton(
                      style: ButtonStyleCustom.outlinedbutton,
                      onPressed: () {
                        final formState = _formKey.currentState;
                        if (formState!.validate()) {
                          dynamic hitung1 =
                              int.parse(currentPasswordController.text) % 3;
                          dynamic hitung2 =
                              int.parse(currentPasswordController.text) % 5;
                          print(hitung1.toString());
                          print(hitung2.toString());
                          if (hitung2 == 0 && hitung1 == 0) {
                            print('satu');
                          } else if (hitung1 == 0 && hitung2 != 0) {
                            print('dua');
                          } else if (hitung2 == 0 && hitung1 != 0) {
                            print('tiga');
                          } else {
                            print('tidak ada return');
                          }
                        } else {
                          return null;
                        }
                      },
                      child: Text('Submit',
                          style: TextStyle(
                              // fontFamily: 'PlusJakartaSans',
                              fontSize: 18,
                              fontWeight: FontWeight.w500)),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    )));
  }
}
