import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_ptgits/main.dart';

class SoalNomersatu extends StatefulWidget {
  const SoalNomersatu({Key? key}) : super(key: key);

  @override
  _SoalNomersatuState createState() => _SoalNomersatuState();
}

class _SoalNomersatuState extends State<SoalNomersatu> {
  TextEditingController currentPasswordController = new TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Container(
        child: SafeArea(
            child: Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 25.0),
        child: Column(
          children: [
            Form(
              key: _formKey,
              child: Column(
                children: [
                  Divider(color: Colors.black, thickness: 2.0),
                  TextFormField(
                      autovalidateMode: AutovalidateMode.always,
                      textAlign: TextAlign.center,
                      controller: currentPasswordController,
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.digitsOnly
                      ],
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'kosong';
                        }
                      },
                      decoration: InputDecoration.collapsed(
                          hintText: "inputkan bilangan, Example : 3")),
                  Divider(color: Colors.black, thickness: 2.0),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: TextButton(
                      style: ButtonStyleCustom.outlinedbutton,
                      onPressed: () {
                        final formState = _formKey.currentState;
                        if (formState!.validate()) {
                          dynamic hitung1 =
                              int.parse(currentPasswordController.text) % 3;
                          dynamic hitung2 =
                              int.parse(currentPasswordController.text) % 5;
                              print(hitung1.toString());
                              print(hitung2.toString());
                          if (hitung2 == 0 && hitung1 == 0) {
                            print('satu');
                          } else if (hitung1 == 0 && hitung2 != 0) {
                            print('dua');
                          } else if (hitung2 == 0 && hitung1 != 0) {
                            print('tiga');
                          } else {
                            print('tidak ada return');
                          }
                        } else {
                          return null;
                        }
                      },
                      child: Text('Submit',
                          style: TextStyle(
                              // fontFamily: 'PlusJakartaSans',
                              fontSize: 18,
                              fontWeight: FontWeight.w500)),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    )));
  }
}
