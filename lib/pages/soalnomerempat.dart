import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_ptgits/main.dart';

class Soalnomerempat extends StatefulWidget {
  const Soalnomerempat({ Key? key }) : super(key: key);

  @override
  _SoalnomerempatState createState() => _SoalnomerempatState();
}

class _SoalnomerempatState extends State<Soalnomerempat> {
   TextEditingController currentPasswordController = new TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String reverseStringUsingCodeUnits(String input) {
  return String.fromCharCodes(input.codeUnits.reversed);
}
  @override
  Widget build(BuildContext context) {
    return Container(
        child: SafeArea(
            child: Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 25.0),
        child: Column(
          children: [
            Form(
              key: _formKey,
              child: Column(
                children: [
                  Divider(color: Colors.black, thickness: 2.0),
                  TextFormField(
                      autovalidateMode: AutovalidateMode.always,
                      textAlign: TextAlign.center,
                      controller: currentPasswordController,
                      keyboardType: TextInputType.text,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'KOSONG';
                        }
                      },
                      decoration: InputDecoration.collapsed(
                          hintText: "inputkan bilangan, Example : 3")),
                  Divider(color: Colors.black, thickness: 2.0),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: TextButton(
                      style: ButtonStyleCustom.outlinedbutton,
                      onPressed: () {
                        final formState = _formKey.currentState;
                        if (formState!.validate()) {
                          print(reverseStringUsingCodeUnits(currentPasswordController.text));
                        } else {
                          return null;
                        }
                      },
                      child: Text('Submit',
                          style: TextStyle(
                              // fontFamily: 'PlusJakartaSans',
                              fontSize: 18,
                              fontWeight: FontWeight.w500)),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    )));
  }
}
