import 'package:flutter/material.dart';
import 'package:flutter_ptgits/main.dart';

class Soalnomerlima extends StatefulWidget {
  const Soalnomerlima({Key? key}) : super(key: key);

  @override
  _SoalnomerlimaState createState() => _SoalnomerlimaState();
}

class _SoalnomerlimaState extends State<Soalnomerlima> {
  TextEditingController currentPasswordController = new TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String reverseStringUsingCodeUnits(String input) {
    return String.fromCharCodes(input.codeUnits.reversed);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SafeArea(
          child: Scaffold(
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
          child: Column(
            children: [
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    Divider(color: Colors.black, thickness: 2.0),
                    TextFormField(
                        autovalidateMode: AutovalidateMode.always,
                        textAlign: TextAlign.center,
                        controller: currentPasswordController,
                        keyboardType: TextInputType.text,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'KOSONG';
                          }
                        },
                        decoration: InputDecoration.collapsed(
                            hintText: "inputkan kalimat")),
                    Divider(color: Colors.black, thickness: 2.0),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: TextButton(
                        style: ButtonStyleCustom.outlinedbutton,
                        onPressed: () {
                          final formState = _formKey.currentState;
                          if (formState!.validate()) {
                            String text = reverseStringUsingCodeUnits(
                                currentPasswordController.text);
                            if (currentPasswordController.text.toLowerCase() == text.toLowerCase()) {
                              print(true);
                            } else {
                            print(false);
                            }
                          } else {
                            return null;
                          }
                        },
                        child: Text('Submit',
                            style: TextStyle(
                                // fontFamily: 'PlusJakartaSans',
                                fontSize: 18,
                                fontWeight: FontWeight.w500)),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      )),
    );
  }
}
