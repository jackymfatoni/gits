import 'package:flutter/material.dart';
import 'package:flutter_ptgits/pages/soalnomerdua.dart';
import 'package:flutter_ptgits/pages/soalnomerempat.dart';
import 'package:flutter_ptgits/pages/soalnomerlima.dart';
import 'package:flutter_ptgits/pages/soalnomersatu.dart';
import 'package:flutter_ptgits/pages/soalnomertiga.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: SafeArea(
            child: Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 25.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              child: TextButton(
                style: ButtonStyleCustom.outlinedbutton,
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => SoalNomersatu()));
                },
                child: Text('Soal nomer satu',
                    style: TextStyle(
                        // fontFamily: 'PlusJakartaSans',
                        fontSize: 18,
                        fontWeight: FontWeight.w500)),
              ),
            ),
            SizedBox(
              height: 50.0,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: TextButton(
                style: ButtonStyleCustom.outlinedbutton,
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => Soalnomerdua()));
                },
                child: Text('Soal nomer dua',
                    style: TextStyle(
                        // fontFamily: 'PlusJakartaSans',
                        fontSize: 18,
                        fontWeight: FontWeight.w500)),
              ),
            ),
            SizedBox(
              height: 50.0,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: TextButton(
                style: ButtonStyleCustom.outlinedbutton,
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => Soalnomertiga()));
                },
                child: Text('Soal nomer tiga (GAGAL)',
                    style: TextStyle(
                        // fontFamily: 'PlusJakartaSans',
                        fontSize: 18,
                        fontWeight: FontWeight.w500)),
              ),
            ),
            SizedBox(
              height: 50.0,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: TextButton(
                style: ButtonStyleCustom.outlinedbutton,
                onPressed: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (_) => Soalnomerempat()));
                },
                child: Text('Soal nomer empat',
                    style: TextStyle(
                        // fontFamily: 'PlusJakartaSans',
                        fontSize: 18,
                        fontWeight: FontWeight.w500)),
              ),
            ),
            SizedBox(
              height: 50.0,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: TextButton(
                style: ButtonStyleCustom.outlinedbutton,
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => Soalnomerlima()));
                },
                child: Text('Soal nomer lima',
                    style: TextStyle(
                        // fontFamily: 'PlusJakartaSans',
                        fontSize: 18,
                        fontWeight: FontWeight.w500)),
              ),
            ),
          ],
        ),
      ),
    )));
  }
}

class ButtonStyleCustom {
  static final outlinedbutton = TextButton.styleFrom(
    primary: Colors.black,
    minimumSize: Size(88, 36),
    padding: EdgeInsets.symmetric(vertical: 15.0),
    shape: new RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(10.0),
        side: BorderSide(color: Color(0Xff375685))),
  );
}
